/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.probelogr.lib.services;

//this line of code below can be removed if you do not have GSON library
import com.google.gson.Gson;
import com.probelogr.lib.services.objects.BaseLog;
import com.probelogr.lib.services.objects.singleton.Singletons;
import com.probelogr.lib.services.tailer.ProbelogrTailListener;
import com.probelogr.lib.services.websocket.ProbeStreamHandler;
import com.probelogr.lib.utils.AbstractObject;
import com.probelogr.lib.utils.Meths;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 *
 *
 * @author uchephilz
 */
public class ProbelogrCore {

    //url
    private final String LOOSE_LOG = "https://api.probelogr.com/logit/loose-log";
    private final String PERSIST_LOG = "https://api.probelogr.com/logit/persist-log";

    private String accessToken;
    private String tag;
    private String logObject;

    private ProbeStreamHandler handler;

    private boolean shouldPersist = false;

    /**
     * Start building your probelogr
     *
     * @return ProbelogrCore
     */
    public static ProbelogrCore startBuilding() {
        return new ProbelogrCore();
    }

    public ProbelogrCore setAccessToken(String accessToken) {
        this.accessToken = accessToken;
        return this;
    }

    public ProbelogrCore setTag(String tag) {
        this.tag = tag;
        return this;
    }

    public ProbelogrCore setShouldPersist(boolean shouldPersist) {
        this.shouldPersist = shouldPersist;
        return this;
    }

    public ProbelogrCore setLogObject(String logObject) {
        this.logObject = logObject;
        return this;
    }

    public ProbelogrCore setStreamHandler(ProbeStreamHandler handler) {
        this.handler = handler;
        return this;
    }

    private void send(String tag, String logObject) {
        if (shouldPersist) {
            handler.sendPersistStreamMessage(tag, logObject);
        } else {
            handler.sendLooseStreamMessage(tag, logObject);
        }
    }

    public void pushEngine() {

        if (Meths.notNull(handler)) {
            send(tag, logObject);
        } else {

            if (Meths.notEmpty(this.tag) && Meths.notNull(this.logObject) && Meths.notEmpty(this.accessToken)) {

                try {
                    String makeBody = new BaseLog(tag, logObject, accessToken).toJson();

                    URL obj = new URL(this.shouldPersist ? PERSIST_LOG : LOOSE_LOG);
                    HttpURLConnection postConnection = (HttpURLConnection) obj.openConnection();
                    postConnection.setRequestMethod("POST");
                    postConnection.setRequestProperty("accessToken", this.accessToken);
                    postConnection.setRequestProperty("Content-Type", "application/json");
                    postConnection.setDoOutput(true);
                    OutputStream os = postConnection.getOutputStream();
                    os.write(makeBody.getBytes());
                    os.flush();
                    os.close();
                    int responseCode = postConnection.getResponseCode();

                    if (responseCode == HttpURLConnection.HTTP_OK) { //success
                        BufferedReader in = new BufferedReader(new InputStreamReader(
                                postConnection.getInputStream()));
                        String inputLine;
                        StringBuilder response = new StringBuilder();
                        while ((inputLine = in.readLine()) != null) {
                            response.append(inputLine);
                        }
                        in.close();
                        System.out.println(response.toString());
                    }
                } catch (MalformedURLException ex) {
                    Logger.getLogger(ProbelogrCore.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(ProbelogrCore.class.getName()).log(Level.SEVERE, null, ex);
                }

            } else {
                System.out.println("Tag, Body or accesstoken not set");
            }

        }

    }
}
