/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.probelogr.lib.services.objects;

import com.google.gson.annotations.SerializedName;
import com.probelogr.lib.services.objects.singleton.Singletons;
import com.probelogr.lib.utils.AbstractObject;
import java.time.LocalDate;
import java.util.Date;

/**
 *
 * This is the wrapper class for every object that should be sent to the probelogr server
 * 
 * @author uchephilz
 */
public class BaseLog {

    private String tag;
    private String logObject;
    private String accessToken;

    /**
     * 
     * construct BaseLog object
     * 
     * @param tag String
     * @param logBody String
     * @param accessToken String
     */
    public BaseLog(String tag, String logBody, String accessToken) {
        this.tag = tag;
        this.logObject = logBody;
        this.accessToken = accessToken;
    }

    /**
     * Get the tag, or return null if tag has not been set
     * @return String | NULL
     */
    public String getTag() {
        return tag;
    }

    /**
     * Set the tag field, tag is null by default
     * @param tag The tag should be from the what you have created on Probelogr
     */
    public void setTag(String tag) {
        this.tag = tag;
    }


    /**
     * Get the logObject, or return null if logObject has not been set
     * @return String | Null
     */
    public String getLogObject() {
        return logObject;
    }

    /**
     * Set the logObject field, logObject is null by default
     * @param logObject String
     */
    public void setLogObject(String logObject) {
        this.logObject = logObject;
    }

    /**
     * Gets the accessToken or return null if accessToken has not been set
     * @return String | null
     */
    public String getAccessToken() {
        return accessToken;
    }

    /**
     * Set the accessToken, default is null if not set
     * @param accessToken String
     */
    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    /**
     * This method converts This BaseLog to Json
     * @return String (Json) of this object
     */
    public String toJson() {
        return Singletons.getGsonInstance().toJson(this);
    }

}
