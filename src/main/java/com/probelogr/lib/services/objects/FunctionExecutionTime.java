/*
 * The MIT License
 *
 * Copyright 2021 uchephilz.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.probelogr.lib.services.objects;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.probelogr.lib.services.ProbelogrCore;
import com.probelogr.lib.services.objects.adapter.RoutingRunTimeAdapter;
import com.probelogr.lib.services.websocket.ProbeStreamHandler;
import com.probelogr.lib.utils.Meths;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author uchephilz
 */
public class FunctionExecutionTime {

    @Expose
    private String accessToken;
    @Expose
    private String tag;
    @Expose
    private String functionName;
    @Expose
    private Map<String, String> inputData = new HashMap<>();
    @Expose
    private long startTime;
    @Expose
    private Map<String, String> outputData = new HashMap<>();
    @Expose
    private long endTime;

    private final String objectType = "FUNCTION-EXECUTION-TIME";

    private TimeElapse timeElapse;

    private ProbeStreamHandler streamHandler;

    /**
     * Begin construction of the FunctionExecutionTime object
     *
     * @return FunctionExecutionTime
     */
    public static FunctionExecutionTime startBuilding() {
        return new FunctionExecutionTime();
    }

    /**
     *
     * @param accessToken String
     * @return FunctionExecutionTime
     */
    public FunctionExecutionTime setAccessToken(String accessToken) {
        this.accessToken = accessToken;
        return this;
    }

    /**
     *
     * @param tag String
     * @return FunctionExecutionTime
     */
    public FunctionExecutionTime setTag(String tag) {
        this.tag = tag;
        return this;
    }

    /**
     * Set the name of the function begin executed
     *
     * @param functionName String
     * @return FunctionExecutionTime
     */
    public FunctionExecutionTime setFunctionName(String functionName) {
        this.functionName = functionName;
        return this;
    }

    /**
     *
     * Set the parameters used to execute the function
     *
     * @param inputMap Map
     * @return FunctionExecutionTime
     */
    public FunctionExecutionTime setInputMap(Map<String, String> inputMap) {
        this.inputData = inputMap;
        return this;
    }

    /**
     *
     * Add individual parameters used to execute the function
     *
     * @param key String
     * @param value String
     * @return FunctionExecutionTime
     */
    public FunctionExecutionTime addInput(String key, String value) {
        this.inputData.put(key, value);
        return this;
    }

    /**
     * This function should be called at the top part of the function
     *
     * @return FunctionExecutionTime
     */
    public FunctionExecutionTime setStartTime() {
        this.startTime = System.currentTimeMillis();
        return this;
    }

    /**
     * Set the values that is returned by the function you executed
     *
     * @param outputData Map
     * @return FunctionExecutionTime
     */
    public FunctionExecutionTime setOutputData(Map<String, String> outputData) {
        this.outputData = outputData;
        return this;
    }

    /**
     * Add value that is returned by the function you executed
     *
     * @param key String
     * @param value String
     * @return FunctionExecutionTime
     */
    public FunctionExecutionTime addOutput(String key, String value) {
        this.outputData.put(key, value);
        return this;
    }

    /**
     * This function should be called at the bottom of the functio
     *
     * @return FunctionExecutionTime
     */
    public FunctionExecutionTime setEndTime() {
        this.endTime = System.currentTimeMillis();
        return this;
    }

    /**
     *
     * @param streamHandler ProbeStreamHandler
     * @return FunctionExecutionTime
     */
    public FunctionExecutionTime setStreamHandler(ProbeStreamHandler streamHandler) {
        this.streamHandler = streamHandler;
        return this;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public String getTag() {
        return tag;
    }

    public String getFunctionName() {
        return functionName;
    }

    public Map<String, String> getInputData() {
        return inputData;
    }

    public long getStartTime() {
        return startTime;
    }

    public Map<String, String> getOutputData() {
        return outputData;
    }

    public long getEndTime() {
        return endTime;
    }

    public String getObjectType() {
        return objectType;
    }

    public TimeElapse getTimeElapse() {
        return timeElapse;
    }

    public ProbeStreamHandler getStreamHandler() {
        return streamHandler;
    }

    /**
     * This is called when all necessary field have been populated, as this push
     * the final data to the server for analysis
     */
    public void push() {

        if (Meths.notEmpty(this.tag)
                && Meths.notNull(this.functionName)
                && Meths.notNull(this.startTime)
                && Meths.notNull(this.endTime)
                && Meths.notEmpty(this.accessToken)) {

            this.timeElapse = new TimeElapse(this.startTime, this.endTime);

            ProbelogrCore pc = ProbelogrCore.startBuilding()
                    .setAccessToken(this.accessToken)
                    .setStreamHandler(streamHandler)
                    .setTag(this.tag);

            this.tag = null;
            this.accessToken = null;

            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.registerTypeAdapter(FunctionExecutionTime.class, RoutingRunTimeAdapter.getInstance());
            Gson gson = gsonBuilder.create();
            String g = gson.toJson(this);

            pc.setLogObject(g)
                    .pushEngine();
        }

    }

}
