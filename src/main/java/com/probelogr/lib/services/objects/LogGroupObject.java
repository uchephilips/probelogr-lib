/*
 * The MIT License
 *
 * Copyright 2021 uchephilz.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.probelogr.lib.services.objects;

import com.probelogr.lib.utils.Meths;

/**
 *
 * @author uchephilz
 */
public class LogGroupObject {

    private String tag;
    private int logSize;
    private String logData;
    private String context;

    public static LogGroupObject startBuilding() {
        return new LogGroupObject();
    }

    public LogGroupObject setTag(String tag) {
        this.tag = tag;
        return this;
    }

    public LogGroupObject setLogSize(int logSize) {
        this.logSize = logSize;
        return this;
    }

    public LogGroupObject setLogData(String logData) {
        this.logData = logData;
        return this;
    }

    public LogGroupObject setContext(String context) {
        this.context = context;
        return this;
    }

    public String getTag() {
        return tag;
    }

    public int getLogSize() {
        return logSize;
    }

    public String getLogData() {
        return logData;
    }

    public String getContext() {
        return context;
    }

}
