/*
 * The MIT License
 *
 * Copyright 2021 uchephilz.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.probelogr.lib.services.objects;

import com.probelogr.lib.utils.Meths;

/**
 *
 * @author uchephilz
 */
public class TimeElapse {

    private long different;

    private long elapsedDays;
    private long elapsedHours = different;
    private long elapsedMinutes;
    private long elapsedSeconds;
    private long elapsedMilliSeconds;

    public TimeElapse(long startDate, long endDate) {

        //milliseconds
        long different = endDate - startDate;

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        this.elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        this.elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        this.elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        this.elapsedSeconds = different / secondsInMilli;

        this.elapsedMilliSeconds = different % secondsInMilli;

    }

    public long getDifferent() {
        return different;
    }

    public long getElapsedDays() {
        return elapsedDays;
    }

    public long getElapsedHours() {
        return elapsedHours;
    }

    public long getElapsedMinutes() {
        return elapsedMinutes;
    }

    public long getElapsedSeconds() {
        return elapsedSeconds;
    }

    public long getElapsedMilliSeconds() {
        return elapsedMilliSeconds;
    }

}
