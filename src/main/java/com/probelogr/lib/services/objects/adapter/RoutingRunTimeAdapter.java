/*
 * The MIT License
 *
 * Copyright 2021 uchephilz.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.probelogr.lib.services.objects.adapter;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.probelogr.lib.services.objects.FunctionExecutionTime;
import com.probelogr.lib.services.objects.singleton.Singletons;
import java.lang.reflect.Type;

/**
 *
 * @author uchephilz
 */
public class RoutingRunTimeAdapter implements JsonSerializer<FunctionExecutionTime> {

    private final static RoutingRunTimeAdapter routingRunTimeAdapter = new RoutingRunTimeAdapter();

    public static RoutingRunTimeAdapter getInstance() {
        return routingRunTimeAdapter;
    }

    @Override
    public JsonElement serialize(FunctionExecutionTime routingRunTime, Type typeOfSrc, JsonSerializationContext context) {

        JsonObject jsonObject = new JsonObject();
//        jsonObject.addProperty("acessToken", routingRunTime.getAccessToken());
//        jsonObject.addProperty("tag", routingRunTime.getTag());
        jsonObject.addProperty("routingName", routingRunTime.getFunctionName());
        jsonObject.addProperty("startTime", routingRunTime.getStartTime());
        jsonObject.add("inputData", Singletons.getGsonInstance().toJsonTree(routingRunTime.getInputData()));
        jsonObject.addProperty("endTime", routingRunTime.getStartTime());
        jsonObject.addProperty("objectType", routingRunTime.getObjectType());
        jsonObject.add("outputData", Singletons.getGsonInstance().toJsonTree(routingRunTime.getOutputData()));
        jsonObject.add("timeElapse", Singletons.getGsonInstance().toJsonTree(routingRunTime.getTimeElapse()));
        return jsonObject;
    }

}
