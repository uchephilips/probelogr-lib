/*
 * The MIT License
 *
 * Copyright 2021 Pivotal Software, Inc..
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.probelogr.lib.services.websocket;

import com.probelogr.lib.services.objects.BaseLog;
import com.google.gson.Gson;
import com.probelogr.lib.services.objects.singleton.Singletons;
import com.probelogr.lib.utils.Meths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import org.springframework.lang.Nullable;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;
import org.springframework.web.socket.client.WebSocketClient;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;
import org.springframework.web.socket.sockjs.client.SockJsClient;
import org.springframework.web.socket.sockjs.client.Transport;
import org.springframework.web.socket.sockjs.client.WebSocketTransport;

/**
 *
 * Probeloger Stream Handlers connects to the Probelogr Server through <br>
 * Web Socket, to allow optimized flow data.
 *
 * @author uchephilz
 */
public class ProbeStreamHandler extends StompSessionHandlerAdapter {

    private StompSession session = null;
    private String accessToken;
    public final String URL = "https://api.probelogr.com/ws";
    private WebSocketStompClient stompClient = null;
    private ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
    private ScheduledFuture<?> scheduleAtFixedRate = null;

    public void setSession(StompSession session) {
        this.session = session;
    }

    public void setStompClient(WebSocketStompClient stompClient) {
        this.stompClient = stompClient;
    }

    public StompSession getSession() {
        return session;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public String getURL() {
        return URL;
    }

    public WebSocketStompClient getStompClient() {
        return stompClient;
    }

    /**
     *
     * @param accessToken token generated from
     */
    public ProbeStreamHandler(String accessToken) {
        this.accessToken = accessToken;
        try {
            stompClient = setupStomp();
            session = stompClient.connect(URL, this).get();
        } catch (InterruptedException | ExecutionException ex) {
            System.out.println("failed to connect to probelogr server");
        }

    }

    private Runnable reconnect(ProbeStreamHandler handler) {
        return new Runnable() {
            @Override
            public void run() {
                try {
                    if (!handler.getSession().isConnected()) {
                        handler.setSession(handler.getStompClient().connect(handler.getURL(), handler).get());
                    } else {
                        disconnect();
                    }
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }
        };
    }

    private WebSocketStompClient setupStomp() {
        WebSocketClient simpleWebSocketClient = new StandardWebSocketClient();
        List<Transport> transports = new ArrayList(1);
        transports.add(new WebSocketTransport(simpleWebSocketClient));
        SockJsClient sockJsClient = new SockJsClient(transports);

        WebSocketStompClient stompClient = new WebSocketStompClient(sockJsClient);
        stompClient.setMessageConverter(new MappingJackson2MessageConverter());

        return stompClient;
    }

    public void sendLooseStreamMessage(String tag, String logObject) {
        send("/app/looseStream", tag, logObject);
    }

    public void sendPersistStreamMessage(String tag, String logObject) {
        send("/app/persistentStream", tag, logObject);
    }

    private void send(String desctination, String tag, String logObject) {
        if (Meths.notNull(session)) {
            if (!session.isConnected()) {
                this.reconnect(this);
            }
            if (session.isConnected()) {
                StompSession.Receiptable send = session.send(desctination, new BaseLog(tag, logObject, accessToken).toJson());
            }
        }
    }

    @Override
    public void afterConnected(StompSession session, StompHeaders connectedHeaders) {
        System.err.println("Connected!");
        if (Meths.notNull(scheduleAtFixedRate) && !scheduleAtFixedRate.isDone()) {
            System.out.println("cancelling reconnection scheduler");
            scheduleAtFixedRate.cancel(true);
        }

    }

    @Override
    public void handleTransportError(StompSession session, Throwable exception) {
        System.out.println("Error on probe stream session " + session.getSessionId());
        if (!Meths.notNull(scheduleAtFixedRate) || scheduleAtFixedRate.isDone()) {
            scheduleAtFixedRate = executorService.scheduleAtFixedRate(this.reconnect(this), 0, 10, TimeUnit.SECONDS);
        } else {
            System.out.println("reconnecting to probelogr server");
        }
    }

    @Override
    public void handleException(StompSession session, @Nullable StompCommand command, StompHeaders headers, byte[] payload, Throwable exception) {
        System.out.println("exception found [" + exception.getMessage() + "]");
    }

    public void disconnect() {
        if (Meths.notNull(session) && session.isConnected()) {
            this.session.disconnect();
        }
    }

}
