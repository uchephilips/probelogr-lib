/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.probelogr.lib.services.websocket;

import com.probelogr.lib.services.tailer.ProbelogrTailer;
import java.io.FileNotFoundException;

/**
 *
 * @author uchephilz
 */
public class SocketStream {

    

    public static void main(String[] args) throws FileNotFoundException, InterruptedException {
        String tag = "LOG-STREAM";
        String accessToken = "617ace2f3768f1-50273075617ace2f376946-75403081";

        ProbelogrTailer.startBuilding()
                .setFile("/var/log.txt")
                .setAccessToken(accessToken)
                .setTag(tag)
                .setShouldPersist(false)
                .shouldLog(true)
                .run();

    }

}
